import React, { Component } from "react";
import PropTypes from 'prop-types'
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from '@material-ui/core/Typography';
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import TextField from "@material-ui/core/TextField";
import StepWizard from "react-step-wizard";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Button from "@material-ui/core/Button";
import Image from '../img/section.jpg';
import Footer from './Footer'


import {
    Card,
    CircularProgress,
    CardContepng,
    CardMedia,
    CardActionArea,
    Fab,
    Dialog,
    DialogContent,
    DialogActions,
    DialogContentText,
    DialogTitle,
} from "@material-ui/core";




const styles = (theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    paperContainer: {

        backgroundImage: `url(${Image})`,
        width: '100%'
    },
    heading: {
        fontSize: '50px',
        textAlign: 'center'
    },
    subheading: {
        textAlign: 'center',
        fontSize: '20px',
        fontWeight: '500'
    },
    blockone: {
        backgroundColor: 'black'
    },
    heading2: {
        fontSize: '25px'
    },
    subheading2: {
        fontSize: '15px'
    },
    imagefile: {
        paddingTop: '3%',
        width: '50%'
    },
    box: {
        height: '330px',
        width: '70%',
        marginTop: '3%',
        marginLeft: '15%',
        backgroundColor: 'white',
        borderRadius: '40px'
    },
    box2: {
        height: '330px',
        width: '70%',
        marginTop: '3%',
        marginLeft: '15%',
        backgroundColor: '#000000b5',
        borderRadius: '40px'
    },


});
class TaskPage extends Component {

    constructor(props) {
        super(props);
        this.state = {

        }
    }




    render() {
        const { classes } = this.props;


        return (
            <div className={classes.root}>
                <Grid container spacing={3} style={{ padding: '1%' }}>
                    <Grid item xs={12} style={{ padding: '5px' }}>
                        <Paper className={classes.paper} className={classes.paperContainer}>
                            <div style={{ textAlign: 'center', padding: '10%' }}>
                                <Typography className={classes.heading}>Punny headline</Typography>
                                <Typography className={classes.subheading}>And an even wittier subheading to boot. Jumpstart
                                </Typography >
                                <Typography className={classes.subheading}>
                                    your marketing efforts with the example based on
                                </Typography>
                                <Typography className={classes.subheading}>
                                    Apple's marketing Pages
                                </Typography>

                                <Button variant="outlined" style={{ textTransform: 'capitalize', marginTop: '2%' }}>
                                    Comming Soon
                                    </Button>
                            </div>




                        </Paper>
                    </Grid>
                    <Grid item xs={6} style={{ padding: '5px' }}>
                        <Paper className={classes.paper} style={{ backgroundColor: '#000000b5' }}>
                            <Typography className={classes.heading2} style={{ color: 'white' }}>Another headline</Typography>
                            <Typography className={classes.subheading2} style={{ color: 'white' }}>And an even wittier subheading</Typography>
                            <div className={classes.box} ></div>
                        </Paper>
                    </Grid>
                    <Grid item xs={6} style={{ padding: '5px' }}>
                        <Paper className={classes.paper} style={{ backgroundColor: '#e0e0e05e' }}>
                            <Typography className={classes.heading2} style={{ color: 'black' }}>Another headline</Typography>
                            <Typography className={classes.subheading2} style={{ color: 'black' }}>And an even wittier subheading</Typography>
                            <div className={classes.box2} ></div>
                        </Paper>
                    </Grid>
                    <Grid item xs={6} style={{ padding: '5px' }}>
                        <Paper className={classes.paper} style={{ backgroundColor: 'white' }}>
                            <Typography className={classes.heading2} style={{ color: 'black' }}>Another headline</Typography>
                            <Typography className={classes.subheading2} style={{ color: 'black' }}>And an even wittier subheading</Typography>
                            <div className={classes.box2} ></div>
                        </Paper>
                    </Grid>
                    <Grid item xs={6} style={{ padding: '5px' }}>

                        <Paper className={classes.paper} style={{ backgroundColor: '#6180fb' }}>
                            <Typography className={classes.heading2} style={{ color: 'white' }}>Another headline</Typography>
                            <Typography className={classes.subheading2} style={{ color: 'white' }}>And an even wittier subheading</Typography>
                            <div className={classes.box} ></div>
                        </Paper>
                    </Grid>

                    <Grid item xs={6} style={{ padding: '5px' }}>
                        <Paper className={classes.paper} style={{ backgroundColor: '#e0e0e05e' }}>
                            <Typography className={classes.heading2} style={{ color: 'black' }}>Another headline</Typography>
                            <Typography className={classes.subheading2} style={{ color: 'black' }}>And an even wittier subheading</Typography>
                            <div className={classes.box} ></div>
                        </Paper>

                    </Grid>
                    <Grid item xs={6} style={{ padding: '5px' }}>
                        <Paper className={classes.paper} style={{ backgroundColor: '#e0e0e05e' }}>
                            <Typography className={classes.heading2} style={{ color: 'black' }}>Another headline</Typography>
                            <Typography className={classes.subheading2} style={{ color: 'black' }}>And an even wittier subheading</Typography>
                            <div className={classes.box} ></div>
                        </Paper>

                    </Grid>

                    <Grid item xs={6} style={{ padding: '5px' }}>
                        <Paper className={classes.paper} style={{ backgroundColor: '#e0e0e05e' }}>
                            <Typography className={classes.heading2} style={{ color: 'black' }}>Another headline</Typography>
                            <Typography className={classes.subheading2} style={{ color: 'black' }}>And an even wittier subheading</Typography>
                            <div className={classes.box} ></div>
                        </Paper>
                    </Grid>
                    <Grid item xs={6} style={{ padding: '5px' }}>
                        <Paper className={classes.paper} style={{ backgroundColor: '#e0e0e05e' }}>
                            <Typography className={classes.heading2} style={{ color: 'black' }}>Another headline</Typography>
                            <Typography className={classes.subheading2} style={{ color: 'black' }}>And an even wittier subheading</Typography>
                            {/* <img className={classes.imagefile} src={Square4} /> */}
                            <div className={classes.box} ></div>
                        </Paper>
                    </Grid>

                </Grid>
                <Footer/>
            </div>

        );
    }
}




export default withStyles(styles)(TaskPage);
