import React from "react";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import IconImg from '../img/clipart.png'
// reactstrap components
import {
    Button,
    NavItem,
    NavLink,
    Nav,
    Container,
    Row,
    Col,
    UncontrolledTooltip
} from "reactstrap";


const styles = (theme) => ({
  container: {
    display: "grid",
    gridTemplateColumns: "repeat(12, 1fr)",
    gridGap: theme.spacing(0)
  },
  paper: {
    padding: theme.spacing(0),
    textAlign: "center",
    color: theme.palette.text.secondary,
    whiteSpace: "nowrap",
    marginBottom: theme.spacing(0)
  },
  divider: {
    margin: theme.spacing(2, 0)
  }
  ,
  heading:{
fontSize:'20px',
color:'black'
  }
  
});

class Footer extends React.Component {
    render() {

        const{classes}=this.props;
        return (
          <footer className="footer">
            <Grid
              container
              spacing={3}
              style={{ marginLeft: "5%", marginRight: "5%" }}
            >
              <Grid item xs={2} >
                <img src={IconImg} height="50px" />
                <Typography style={{ color: "gray" }}>© 2017-2018</Typography>
              </Grid>
              <Grid item xs={2}>
                <Typography className={classes.heading}>Features</Typography>
                <Typography style={{ color: "gray" }}>Features</Typography>
                <Typography style={{ color: "gray" }}>Cool Stuff</Typography>
                <Typography style={{ color: "gray" }}>
                  Random feature
                </Typography>
                <Typography style={{ color: "gray" }}>Team feature</Typography>
                <Typography style={{ color: "gray" }}>
                  Stuff for developers
                </Typography>
                <Typography style={{ color: "gray" }}>Last time</Typography>
              </Grid>
              <Grid item xs={2}>
                <Typography className={classes.heading}>Resources</Typography>
                <Typography style={{ color: "gray" }}>Resource</Typography>
                <Typography style={{ color: "gray" }}>Resource name</Typography>
                <Typography style={{ color: "gray" }}>
                  Another resource
                </Typography>
                <Typography style={{ color: "gray" }}>
                  Final resource
                </Typography>
              </Grid>
              <Grid item xs={2}>
                <Typography className={classes.heading}>Resources</Typography>
                <Typography style={{ color: "gray" }}>Bussiness</Typography>
                <Typography style={{ color: "gray" }}>Education</Typography>
                <Typography style={{ color: "gray" }}>Government</Typography>
                <Typography style={{ color: "gray" }}>Gaming</Typography>
              </Grid>
              <Grid item xs={2}>
                <Typography className={classes.heading}>About</Typography>
                <Typography style={{ color: "gray" }}>Team</Typography>
                <Typography style={{ color: "gray" }}>Locations</Typography>
                <Typography style={{ color: "gray" }}>Privacy</Typography>
                <Typography style={{ color: "gray" }}>Terms</Typography>
              </Grid>
              <Grid item xs={2}></Grid>
              <Grid item xs={2}></Grid>
            </Grid>
          </footer>
        );
    }
}

export default withStyles(styles)(Footer);
