import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { deleteContact ,getContact} from '../../actions/contactActions';
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import {
  Card,
  CircularProgress,
  CardContent,
  CardMedia,
  CardActionArea,
  Fab,
  Dialog,
  DialogContent,
  DialogActions,
  DialogContentText,
  DialogTitle
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import AddContact from '../contacts/AddContact'


class Contact extends Component {
  state = {
    showContactInfo: false,
    showstate:false
    
  };
  

  onDeleteClick = id => {
    this.props.deleteContact(id);
    console.log('di',id);
    
  };
 


  render() {
   
    const { id, name, email, phone,address } = this.props.contact;
    const { showContactInfo } = this.state;

    return (
      <div className="card card-body mb-3">
     
         
       

        <Table>
          <TableBody style={{ backgroundColor: "#FFFFFF" }}>
           
                <TableRow>
              <TableCell align="center">  {name}</TableCell>
                  <TableCell align="center">{email}</TableCell>
                  <TableCell align="center">
                    {address.suite},{address.street},{address.city},
                        {address.zipcode}
                  </TableCell>
                  <TableCell align="center">{phone}</TableCell>
                  <TableCell align="center" color="primary">
                <Link to={`contact/edit/${id}`}>
                  <IconButton style={{ color: "#ffad17" }}>
                      <EditIcon />
                      </IconButton>
                   </Link>

                <IconButton style={{ color: "red" }} onClick={this.onDeleteClick.bind(this, id)}>
                      <DeleteIcon />
                    </IconButton>
                  </TableCell>
                </TableRow>
             
          </TableBody>
        </Table>
        {showContactInfo ? (
          <ul className="list-group">
            <li className="list-group-item">Email: {email}</li>
            <li className="list-group-item">Phone: {phone}</li>
          </ul>
        ) : null}
      </div>
    );
  }
}

Contact.propTypes = {
  contact: PropTypes.object.isRequired,
  deleteContact: PropTypes.func.isRequired
};

export default connect(null, { deleteContact })(Contact);
