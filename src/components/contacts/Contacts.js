import React, { Component } from 'react';
import Contact from './Contact';
import { connect } from 'react-redux';
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import {
  Card,
  CircularProgress,
  CardContent,
  CardMedia,
  CardActionArea,
  Fab,
  Dialog,
  DialogContent,
  DialogActions,
  DialogContentText,
  DialogTitle
} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import PropTypes from 'prop-types';
import { getContacts } from '../../actions/contactActions';
import App from '../../App.css'

// import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

class Contacts extends Component {

  constructor(){
    super();
    this.state={
      activePage: 2
    }
  }
  componentDidMount() {
    this.props.getContacts();
  }

  render() {
    const { contacts } = this.props;
    const options = {
      page: 2, // which page you want to show as default
      sizePerPageList: [
        {
          text: "5",
          value: 5
        },
        {
          text: "10",
          value: 10
        },
        {
          text: "All",
          value: contacts.length
        }
      ], // you can change the dropdown list for size per page
      sizePerPage: 5, // which size per page you want to locate as default
      pageStartIndex: 0, // where to start counting the pages
      paginationSize: 3, // the pagination bar size.
      prePage: "Prev", // Previous page button text
      nextPage: "Next", // Next page button text
      firstPage: "First", // First page button text
      lastPage: "Last", // Last page button text
      paginationShowsTotal: this.renderShowsTotal, // Accept bool or function
      paginationPosition: "top" // default is bottom, top and both is all available
      // hideSizePerPage: true > You can hide the dropdown for sizePerPage
      // alwaysShowAllBtns: true // Always show next and previous button
      // withFirstAndLast: false > Hide the going to First and Last page button
    };
    return (
      <React.Fragment>
        <Typography style={{ textAlign: "center",color:'purple',fontSize:'40px' }}>Employees Contact Application</Typography>
        
        <Grid item xs={12}>
          
            <Table style={{ backgroundColor: "#022859" }}>
              <TableHead>
                <TableRow>
                  <TableCell
                    align="center"
                    style={{ fontSize: "15px", color: "#FFFFFF" }}
                  >
                    Name
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{ fontSize: "15px", color: "#FFFFFF" }}
                  >
                    Email
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{ fontSize: "15px", color: "#FFFFFF" }}
                  >
                    Address
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{ fontSize: "15px", color: "#FFFFFF" }}
                  >
                    Phone
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{ fontSize: "15px", color: "#FFFFFF" }}
                  >
                    Actions
                  </TableCell>
                </TableRow>
              </TableHead>
              </Table>
        {contacts.map(contact => (
          <Contact key={contact.id} contact={contact} />
        ))}

       

        {/* <BootstrapTable
          data={contacts}
          pagination={true}
          options={options}
          pagination
        >
          <TableHeaderColumn dataField="id" isKey>
            Product ID
          </TableHeaderColumn>
          <TableHeaderColumn dataField="name">Product Name</TableHeaderColumn>
          <TableHeaderColumn dataField="price">Product Price</TableHeaderColumn>
        </BootstrapTable> */}
        </Grid>
      </React.Fragment>
    );
  }
}

Contacts.propTypes = {
  contacts: PropTypes.array.isRequired,
  getContacts: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  contacts: state.contact.contacts
});

export default connect(mapStateToProps, {getContacts})(Contacts);
