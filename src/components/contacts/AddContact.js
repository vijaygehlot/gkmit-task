import React, { Component } from "react";

import {addContact} from '../../actions/contactActions'
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { makeStyles, withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import TextField from "@material-ui/core/TextField";
import Toolbar from "@material-ui/core/Toolbar";
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import StepWizard from "react-step-wizard";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import RemoveRedEyeIcon from "@material-ui/icons/RemoveRedEye";
import {
  Card,
  CircularProgress,
  CardContent,
  CardMedia,
  CardActionArea,
  Fab,
  Dialog,
  DialogContent,
  DialogActions,
  DialogContentText,
  DialogTitle
} from "@material-ui/core";

const styles = theme => ({
  table: {
    minWidth: 450
  }
});

class AddContact extends React.Component {
  constructor() {
    super();
    this.state = {
      circle: false,
      activeStep: 0,
      stepperStep: 0,
      show: false,
      getSteps: ["Personal Details", "Address Details", "Company Details"],
      name: "",
      username: "",
      address: {
        street: "",
        suite: "",
        city: "",
        zipcode: "",
        geo: {
          lat: "-37.3159",
          lng: "81.1496"
        }
      },
      email: "",
      phone: "",
      errors: {},
      website: "",
      company: {
        name: "",
        catchPhrase: "",
        bs: ""
      }
    };
  }

  handleNext = () => {
    window.scrollTo(0, 0);
    this.setState({ activeStep: this.state.activeStep + 1 });
    console.log("this.state.activeStep", this.state.activeStep);
  };

  handleBack = () => {
    window.scrollTo(0, 0);
    this.setState({ activeStep: this.state.activeStep - 1 });
  };

  handleBasicChange = e => {
    // this.setState({ [e.target.name]: event.target.value, edit: true});
    this.setState({ [e.target.name]: e.target.value });
  };

  handleChange = ({ target }) => {
    const { address } = this.state;

    this.setState(preState => ({
      address: {
        ...preState.address,
        [target.name]: target.value
      }
    }));
  };
  handleComChange = ({ target }) => {
    const { company } = this.state;

    this.setState(preState => ({
      company: {
        ...preState.company,
        [target.name]: target.value
      }
    }));
  };

  SubmitStudentDetails() {
    const {
      name,
      email,
      phone,
      username,
      address,
      company,
      website,
      circle
    } = this.state;
    this.setState({ circle:true})

    // Check For Errors
    if (name === "") {
      this.setState({ errors: { name: "Name is required" } });
      return;
    }

    if (email === "") {
      this.setState({ errors: { email: "Email is required" } });
      return;
    }

    if (phone === "") {
      this.setState({ errors: { phone: "Phone is required" } });
      return;
    }


    const newContact = {
      name,
      username,
      email,
      phone,
      address,
      company,
      website
    };

      this.props.addContact(newContact);

  
  this.setState({
    name: "",
    email: "",
    phone: "",
    username: "",
    errors: {},
    address: {},
    website: "",
    company: {},
    circle: false
  });
  

  this.props.history.push("/");

    // Clear State
    this.setState({
      name: "",
      email: "",
      phone: "",
      username: "",
      errors: {},
      address: {},
      website: "",
      company: {},

    });

    console.log("newContact", newContact);


  }


  render() {
    const { classes } = this.props;
    const {
      getSteps,
      activeStep,
      stepperStep,
      name,
      username,
      email,
      phone,
      address,
      company,
      website,
      circle
    } = this.state;
    console.log("showDailog", this.props);

    return (
      
      <Grid spacing={12} container xs={12} style={{paddingTop:'5%',textAlign:'center',marginLeft:'15%'}} >
      
         
        <h1>Add Employees</h1>
          
          <Stepper
            activeStep={this.state.activeStep}
            style={{ background: "transparent" }}
          >
            {getSteps.map((label, i) => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
          
            <StepWizard>
              {activeStep == 1 ? (
                <Grid container spacing={4}>
              <Grid item md={12} sm={12} xs={12}>
                    <TextField
                     fullWidth
                      id="outlined-basic"
                      onChange={this.handleChange.bind(this)}
                      value={address.street}
                      name="street"
                      label="Street"
                      variant="outlined"
                    />
                  </Grid>
              <Grid item md={12} sm={12} xs={12}>
                    <TextField
                  fullWidth
                      id="outlined-basic"
                      onChange={this.handleChange.bind(this)}
                      value={address.suite}
                      name="suite"
                      label="Suite"
                      variant="outlined"
                    />
                  </Grid>
              <Grid item md={12} sm={12} xs={12}>
                    <TextField
                  fullWidth
                      id="outlined-basic"
                      onChange={this.handleChange.bind(this)}
                      value={address.city}
                      name="city"
                      label="City"
                      variant="outlined"
                    />
                  </Grid>
              <Grid item md={12} sm={12} xs={12}>
                    <TextField
                  fullWidth
                      id="outlined-basic"
                      label="Zipcode"
                      onChange={this.handleChange.bind(this)}
                      value={address.zipcode}
                      name="zipcode"
                      variant="outlined"
                    />
                  </Grid>
                </Grid>
              ) : null}

              {activeStep == 2 ? (
                <Grid container spacing={4}>
              <Grid item md={12} sm={12} xs={12}>
                    <TextField
                  fullWidth
                      id="outlined-basic"
                      onChange={this.handleBasicChange}
                      value={website}
                      name="website"
                      label="Website"
                      variant="outlined"
                    />
                  </Grid>
              <Grid item md={12} sm={12} xs={12}>
                    <TextField
                  fullWidth
                      id="outlined-basic"
                      onChange={this.handleComChange.bind(this)}
                      value={company.name}
                      name="name"
                      label="Company Name"
                      variant="outlined"
                    />
                  </Grid>
              <Grid item md={12} sm={12} xs={12}>
                    <TextField
                  fullWidth
                      id="outlined-basic"
                      onChange={this.handleComChange.bind(this)}
                      value={company.catchPhrase}
                      name="catchPhrase"
                      label="catchPhrase"
                      variant="outlined"
                    />
                  </Grid>
              <Grid item md={12} sm={12} xs={12}>
                    <TextField
                  fullWidth
                      id="outlined-basic"
                      label="bs"
                      onChange={this.handleComChange.bind(this)}
                      value={company.bs}
                      name="bs"
                      variant="outlined"
                    />
                  </Grid>
                </Grid>
              ) : (
                  <Grid container spacing={4}>
                <Grid item md={12} sm={12} xs={12}>
                      <TextField
                        id="outlined-basic"
                    fullWidth
                        onChange={this.handleBasicChange}
                        value={name}
                        name="name"
                        label="Name"
                        variant="outlined"
                      />
                    </Grid>
                <Grid item md={12} sm={12} xs={12}>
                      <TextField
                        id="outlined-basic"
                    fullWidth
                        onChange={this.handleBasicChange}
                        value={username}
                        name="username"
                        label="User Name"
                        variant="outlined"
                      />
                    </Grid>
                <Grid item md={12} sm={12} xs={12}>
                      <TextField
                        id="outlined-basic"
                    fullWidth
                        onChange={this.handleBasicChange}
                        value={email}
                        name="email"
                        label="Email"
                        variant="outlined"
                      />
                    </Grid>
                <Grid item md={12} sm={12} xs={12}>
                      <TextField
                        id="outlined-basic"
                    fullWidth
                        label="Phone"
                        onChange={this.handleBasicChange}
                        value={phone}
                        name="phone"
                        variant="outlined"
                      />
                    </Grid>
                  </Grid>
                )}
            </StepWizard>
         <Grid item sm={12} xs={12} md={12} style={{marginRight:'30%', marginTop:'2%'}}>
            <Button onClick={this.handleBack.bind(this)} color="primary" variant="outlined">
              Back
            </Button>

            {activeStep == 2 ? (
              <Button
                onClick={this.SubmitStudentDetails.bind(this)}
                color="primary"
              style={{ marginLeft: '2%' }}
                autoFocus
              variant="outlined"
              >
                Save
              </Button>
            ) : (
                <Button
                style={{marginLeft:'2%'}}
                  onClick={this.handleNext.bind(this)}
                  color="primary"
                  autoFocus
                variant="outlined"
                >
                  Next
                </Button>
              )}
        </Grid>
      </Grid>
    );
  }
}


AddContact.propTypes = {
  addContact: PropTypes.func.isRequired
}


export default connect(null, { addContact })(AddContact);




// onSubmit = e => {
//   e.preventDefault();

//   const {
//     name,
//     email,
//     phone,
//     username,
//     address,
//     company,
//     website
//   } = this.state;

//   // Check For Errors
//   if (name === "") {
//     this.setState({ errors: { name: "Name is required" } });
//     return;
//   }

//   if (email === "") {
//     this.setState({ errors: { email: "Email is required" } });
//     return;
//   }

//   if (phone === "") {
//     this.setState({ errors: { phone: "Phone is required" } });
//     return;
//   }

//   const newContact = {
//     name,
//     username,
//     email,
//     phone,
//     address,
//     company,
//     website
//   };

//   this.props.addContact(newContact);

//   // Clear State
//   this.setState({
//     name: "",
//     email: "",
//     phone: "",
//     username: "",
//     errors: {},
//     address: {},
//     website: "",
//     company: {}
//   });
//   console.log("newContact", newContact);

//   this.props.history.push("/");
// };